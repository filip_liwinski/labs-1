﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Flight);
        public static Type B = typeof(Passenger);
        public static Type C = typeof(Cargo);

        public static string commonMethodName = "Departure";
    }
}
