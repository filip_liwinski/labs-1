﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    public class Flight
    {
        private int flightNumber;
        private double fuel;
        private double flightTime;

        public Flight(int flightNumber, double fuel, double flightTime)
        {
            this.fuel = fuel;
            this.flightTime = flightTime;
            this.flightNumber = flightNumber;
        }

        public Flight()
        {
            flightNumber = 0;
            fuel = 0;
            flightTime = 0;
        }

        public virtual Flight Departure()
        {
            return new Flight();
        }

    }
}
