﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class Cargo: Flight
    {
        double cargoWeight;

        public Cargo()
        {

            this.cargoWeight = 0;
        }

        public override Flight Departure()
        {
            return new Cargo();
        }

    }
}
