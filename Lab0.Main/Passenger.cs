﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class Passenger : Flight
    {
        int passengersNumber;

        public Passenger()
        {
            
            this.passengersNumber = 0;
        }

        public Passenger(int passengersNumber, int flightNumber, double flightTime, double fuel):base(flightNumber, fuel, flightTime)
        {
            this.passengersNumber = passengersNumber;
        }

        public override Flight Departure()
        {
            return new Passenger();
        }
    }
}
